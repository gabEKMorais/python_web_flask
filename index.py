from flask import Flask

app = Flask(__name__)

@app.route("/<string:nome>")
def getAluno(nome):
    return '<h1> Nome do aluno: {} '.format(nome) + '</h1>'

@app.route("/<string:nome>/<int:nota>")
def getNota(nome, nota):
    if (nota > 7):
        mensagem = 'passou, parabens!'
    else:
        mensagem = 'infelizmente não passou dessa vez..'
    return '<h1>{} você '.format(nome) + mensagem + '</h1>'


app.run(debug=True)